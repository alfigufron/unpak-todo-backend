FROM node:16-alpine

WORKDIR /app/unpak-todo-backend

COPY . /app/unpak-todo-backend

RUN npm install

RUN npm run build

CMD ["node", "build/app.js"]