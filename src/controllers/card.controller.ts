import { NextFunction, Request, Response } from "express";
import AppDataSource from "../config/data-source";
import { ErrorHandler, HttpResponse } from "../config/http";
import { HTTPCode } from "../constant/http.constant";
import { Card } from "../entity/Card";

export default class CardController {
  public static async all(req: Request, res: Response, next: NextFunction) {
    try {
      // const cards = await Card.find();
      const cards = await AppDataSource.query(`SELECT * FROM cards`);

      return HttpResponse.success(res, "List Card", cards);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { name } = req.body;

      // const card = new Card();
      // card.name = name;

      // await card.save();

      AppDataSource.query(`INSERT INTO cards VALUES (NULL, '${name}')`);

      return HttpResponse.success(res, "Create Card Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async detail(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      // const card = await Card.findOneBy({ id: Number(id) });
      const card = await AppDataSource.query(
        `SELECT * FROM cards WHERE id=${id}`
      );

      if (!card)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      return HttpResponse.success(res, "Detail Card", card);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name } = req.body;

      // const card = await Card.findOneBy({ id: Number(id) });
      const card = await AppDataSource.query(
        `SELECT * FROM cards WHERE id=${id}`
      );

      if (!card)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      // card.name = name;
      // card.save();

      AppDataSource.query(`UPDATE cards SET name='${name}' WHERE id=${id}`);

      return HttpResponse.success(res, "Update Card Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      // const card = await Card.findOneBy({ id: Number(id) });
      const card = await AppDataSource.query(
        `SELECT * FROM cards WHERE id=${id}`
      );

      if (!card)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      // card.remove();
      AppDataSource.query(`DELETE FROM cards WHERE id=${id}`);

      return HttpResponse.success(res, "Delete Card Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }
}
