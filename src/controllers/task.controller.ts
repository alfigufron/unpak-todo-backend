import { NextFunction, Request, Response } from "express";
import { ErrorHandler, HttpResponse } from "../config/http";
import { HTTPCode } from "../constant/http.constant";
import { Task } from "../entity/Task";

export default class TaskController {
  public static async all(req: Request, res: Response, next: NextFunction) {
    try {
      const { card_id } = req.params;

      const tasks = await Task.findBy({ card_id: Number(card_id) });

      return HttpResponse.success(res, "List Task", tasks);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { card_id } = req.params;
      const { name } = req.body;

      const task = new Task();
      task.name = name;
      task.check = 0;
      task.card_id = Number(card_id);

      await task.save();

      return HttpResponse.success(res, "Create Task Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async detail(req: Request, res: Response, next: NextFunction) {
    try {
      const { card_id, id } = req.params;

      const task = await Task.findOneBy({
        id: Number(id),
        card_id: Number(card_id),
      });

      if (!task)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      return HttpResponse.success(res, "Detail Task", task);
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name, check } = req.body;

      const task = await Task.findOneBy({ id: Number(id) });

      if (!task)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      task.name = name;
      task.check = check;
      task.save();

      return HttpResponse.success(res, "Update Task Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }

  public static async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;

      const task = await Task.findOneBy({ id: Number(id) });

      if (!task)
        throw new ErrorHandler("Data Not Found", null, HTTPCode.NotFound);

      task.remove();

      return HttpResponse.success(res, "Delete Task Successfully");
    } catch (err) {
      next(new ErrorHandler(err.message, err.data, err.status));
    }
  }
}
