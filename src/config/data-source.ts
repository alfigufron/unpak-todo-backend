import { DataSource } from "typeorm";
import { Card } from "../entity/Card";
import { Task } from "../entity/Task";
import env from "./env";

const AppDataSource = new DataSource({
  type: "mysql",
  host: env.DB.HOST,
  username: env.DB.USERNAME,
  password: env.DB.PASSWORD,
  port: env.DB.PORT,
  database: env.DB.NAME,
  entities: [Card, Task],
});

export default AppDataSource;
