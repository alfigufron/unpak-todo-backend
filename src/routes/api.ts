import { Request, Response, Router } from "express";
import CardController from "../controllers/card.controller";
import TaskController from "../controllers/task.controller";
import UserController from "../controllers/user.controller";

const route = Router();

export default function apiRouter() {
  route.get("/ping", (req: Request, res: Response) => res.send("Service OK!"));

  /**
   * API Version 1
   */
  const apiv1Router = () => {
    route.get("/card", CardController.all);
    route.post("/card", CardController.create);
    route.get("/card/:id", CardController.detail);
    route.put("/card/:id", CardController.update);
    route.delete("/card/:id", CardController.delete);

    route.get("/card/:card_id/task", TaskController.all);
    route.post("/card/:card_id/task", TaskController.create);
    route.get("/card/:card_id/task/:id", TaskController.detail);
    route.put("/card/:card_id/task/:id", TaskController.update);
    route.delete("/card/:card_id/task/:id", TaskController.delete);

    return route;
  };

  route.use("/api/v1", apiv1Router());

  return route;
}
